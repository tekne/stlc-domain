import StlcDomain.Wk

inductive Ty: Type
| unit
| prod (A B : Ty)
| coprod (A B: Ty)
| arrow (A B: Ty)

inductive Term: Type
| var (n: Nat)
| lam (A: Ty) (s: Term)
| app (l r: Term)
| fix (f: Term)
| nil
| pair (l r: Term)
| pi1 (e: Term)
| pi2 (e: Term)
| inl (e: Term)
| inr (e: Term)
| cases (e l r: Term)

def Term.shift: Term -> Wk -> Term
| var n, ρ => var (ρ.var n)
| lam A s, ρ => lam A (s.shift ρ.lift)
| app l r, ρ => app (l.shift ρ) (r.shift ρ)
| fix f, ρ => fix (f.shift ρ)
| nil, _ => nil
| pair l r, ρ => pair (l.shift ρ) (r.shift ρ)
| pi1 e, ρ => pi1 (e.shift ρ)
| pi2 e, ρ => pi2 (e.shift ρ)
| inl e, ρ => inl (e.shift ρ)
| inr e, ρ => inr (e.shift ρ)
| cases e l r, ρ => cases (e.shift ρ) (l.shift ρ.lift) (r.shift ρ.lift)

def Term.wk1 (e: Term) := e.shift Wk.wk1

def Subst := Nat -> Term

def Subst.lift (σ: Subst): Subst
| 0 => Term.var 0
| n + 1 => (σ n).wk1

def Wk.to_subst (ρ: Wk): Subst := λn => Term.var (ρ.var n)

def Term.subst: Term -> Subst -> Term
| var n, σ => σ n
| lam A s, σ => lam A (s.subst σ.lift)
| app l r, σ => app (l.subst σ) (r.subst σ)
| fix f, σ => fix (f.subst σ)
| nil, _ => nil
| pair l r, σ => pair (l.subst σ) (r.subst σ)
| pi1 e, σ => pi1 (e.subst σ)
| pi2 e, σ => pi2 (e.subst σ)
| inl e, σ => inl (e.subst σ)
| inr e, σ => inr (e.subst σ)
| cases e l r, σ => cases (e.subst σ) (l.subst σ.lift) (r.subst σ.lift)

def Context := List Ty

inductive HasVar: Context -> Nat -> Ty -> Prop
| zero {A} {Γ: Context}: HasVar (A::Γ) 0 A
| succ {n A B} {Γ: Context}: HasVar Γ n A -> HasVar (B::Γ) n.succ A 

inductive HasType: Context -> Term -> Ty -> Prop
| var {Γ n A}: HasVar Γ n A -> HasType Γ (Term.var n) A
| lam {Γ} {A B: Ty} {s: Term}: HasType (A::Γ) s B 
  -> HasType Γ (Term.lam A s) (A.arrow B)
| app {Γ} {A B} {l r}: HasType Γ l (A.arrow B) 
  -> HasType Γ r A 
  -> HasType Γ (Term.app l r) B
| fix {Γ A} {s : Term}: HasType Γ s (A.arrow A) -> HasType Γ (Term.fix s) A
| nil: HasType Γ Term.nil Ty.unit
| pair {Γ A B l r}: HasType Γ l A -> HasType Γ r B -> HasType Γ (l.pair r) (A.prod B) 
| pi1 {Γ e} {A B: Ty}: HasType Γ e (A.prod B) -> HasType Γ e.pi1 A
| pi2 {Γ e} {A B: Ty}: HasType Γ e (A.prod B) -> HasType Γ e.pi2 B
| inl {Γ e} {A B: Ty}: HasType Γ e A -> HasType Γ e.inl (A.coprod B)
| inr {Γ e} {A B: Ty}: HasType Γ e B -> HasType Γ e.inl (A.coprod B)
| cases {Γ e l r} {A B C: Ty}:
  HasType Γ e (A.coprod B) ->
  HasType (A::Γ) l C ->
  HasType (B::Γ) r C ->
  HasType Γ (e.cases l r) C

notation Γ " ⊢ " a " : " A => HasType Γ a A